FROM devkitpro/devkitarm:20200730

ENV PATH /opt/devkitpro/devkitARM/bin:$HOME/bin:/usr/local/bin:$PATH

RUN apt-get update
RUN apt-get install doxygen cppcheck -y